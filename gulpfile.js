#!/usr/bin/env node
'use strict';
// =============================================================================
// Configuration Block

// The directory of gulpfiles to source.
var TASKS_DIR = 'build_tasks/';

// The tasks to run when starting over due to gulpfile changes
var DEFAULT_TASKS = ['build-serve'];

// =============================================================================


var gulp = require('gulp');
var childProcess = require('child_process');
var path = require('path');

gulp.task('default', ['watch-gulpfiles']);

// Reload everything if any gulpfiles change
gulp.task('watch-gulpfiles', function() {
  var p;

  gulp.watch(['gulpfile.js', path.join(TASKS_DIR, '**/*')], spawnRealTask);
  spawnRealTask();

  function spawnRealTask() {
    // kill previous spawned process
    if(p) { p.kill(); }
    // `spawn` a child `gulp` process linked to the parent `stdio`
    p = childProcess.spawn('npm', ['run', 'gulp', '--'].concat(DEFAULT_TASKS), {stdio: 'inherit'});
  }
});

var requireDir = require('require-dir');
requireDir(TASKS_DIR);
