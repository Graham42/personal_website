'use strict';

var gulp = require('gulp');

/* This task doesn't need to be run unless the favicon changes, which will be
 * rare, or if a new size is needed, for example, by some new browser spec.
 *
 * Also, this task is complete overkill for a personal website, but it was a
 * good learning experience.
 */
gulp.task('favicons', function (cb) {
  var iconDir = 'src/favicons/';
  var htmlDir = 'src/templates/favicons.html';

  var rmrf = require('rimraf');
  rmrf.sync(iconDir);
  rmrf.sync(htmlDir);

  var favicons = require('favicons');
  favicons({
    files: {
      src: {
        android:      'src/raw-assets/favicon.svg',
        appleIcon:    'src/raw-assets/favicon.svg',
        favicons:     'src/raw-assets/favicon.svg',
        firefox:      'src/raw-assets/favicon.svg',
        windows:      'src/raw-assets/favicon.transparent.svg',
      },
      dest: iconDir,
      html: htmlDir,
      iconsPath: '/',
    },
    icons: {
      android:      true,      // Create Android homescreen icon. `boolean`
      appleIcon:    true,      // Create Apple touch icons. `boolean`
      appleStartup: false,     // Create Apple startup images. `boolean`
      coast:        false,     // Create Opera Coast icon. `boolean`
      favicons:     true,      // Create regular favicons. `boolean`
      firefox:      true,      // Create Firefox OS icons. `boolean`
      opengraph:    false,     // Create Facebook OpenGraph. `boolean`
      windows:      true,      // Create Windows 8 tiles. `boolean`
      yandex:       false,     // Create Yandex browser icon. `boolean`
    },
    settings: {
      appName: 'Graham McGregor',
      background: '#262626',
      logging: true,
    },
  }, cb);
});
