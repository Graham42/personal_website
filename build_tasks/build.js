'use strict';

// gulp imports
var gulp = require('gulp');
var gulpsync = require('gulp-sync')(gulp);
var autoprefixer = require('gulp-autoprefixer');
var CacheBuster = require('gulp-cachebust');
var ignore = require('gulp-ignore');
var cssnano = require('gulp-cssnano');
var nunjucks = require('gulp-nunjucks-html');
var rimraf = require('gulp-rimraf');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
// other npm imports
var browserSync = require('browser-sync');
var mergeStream = require('merge-stream');
// core node.js imports
var path = require('path');


var OUTDIR = '_output/';
// config file is optional so try/catch
try {
  if (process.env['BUILD_DIR']) {
    OUTDIR = process.env['BUILD_DIR'];
  }
} catch (e) {}

// summary builds
gulp.task('build-serve', gulpsync.sync(['clean', 'build', 'serve-watch']));
gulp.task('build', ['templates', 'staticAssets']);

gulp.task('serve-watch', function serveWatch() {
  browserSync({
    open: false,
    server: {
      baseDir: OUTDIR,
    }
  });
  gulp.watch('src/**/*', ['build', browserSync.reload]);
});

gulp.task('clean', function clean() {
 return gulp.src(path.join(OUTDIR, '*'), { read: false }) // much faster
   .pipe(ignore('.git/'))
   .pipe(rimraf());
});

var cacheBuster = new CacheBuster();

gulp.task('templates', ['cssProcessing'], function templates(){
  return gulp.src('src/templates/index.html')
    .pipe(nunjucks({
      searchPaths: ['src/templates']
    }))
    .pipe(cacheBuster.references())
    .pipe(gulp.dest(OUTDIR));
});

gulp.task('cssProcessing', function cssProcessing(){
  return gulp.src('src/css/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'],
        cascade: false
    }))
    .pipe(cssnano())
    .pipe(cacheBuster.resources())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.join(OUTDIR, 'css')));
});

gulp.task('staticAssets', function staticAssets(){
  var s1 = gulp.src(['src/static/**/*', 'src/favicons/*'])
    .pipe(gulp.dest(OUTDIR));

  var s2 = gulp.src(['src/css/font-awesome/fonts/*'])
    .pipe(gulp.dest(path.join(OUTDIR, 'css/fonts/')));

  return mergeStream(s1, s2);
});
