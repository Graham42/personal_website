#!/bin/bash
# Build locally and then transfer files to live server

HOST=$(git config --get remote.live.url | sed 's!ssh://\([^:]*\).*!\1!')
PORT=$(git config --get remote.live.url | sed 's!ssh://[^:]*:\([^/]*\).*!\1!')
if [ "x$HOST" == "x" ]; then
   echo "No remote 'live' found."
   exit 1
fi
BUILD_DIR=_output
TAR_NAME=site.tgz
LIVE_DIR=/home/www/grahammcgregor.ca

# exit script on any failed command
set -e

# make sure can connect before doing anything
ssh $HOST -p $PORT 'mkdir -p ~/tmp/'

BUILD_DIR=$BUILD_DIR npm run build

echo "Packing and sending to server..."
rm -f $TAR_NAME
tar -czf $TAR_NAME $BUILD_DIR

scp -P $PORT $TAR_NAME ${HOST}:~/tmp/$TAR_NAME

# -t for interactive
ssh -t $HOST -p $PORT "
set -e
cd ~/tmp
rm -rf $BUILD_DIR
echo 'Unpacking...'
tar -xzf $TAR_NAME
chmod -R g+w $BUILD_DIR
sudo chown -R www.www $BUILD_DIR
echo 'Updating live files'
rm -rf $LIVE_DIR
mv $BUILD_DIR $LIVE_DIR
echo 'Complete!'
"

